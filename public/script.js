var app = new Vue({
  el: '#contact-form',
  data: {
    form: {
      name: null,
      email: null,
      message: null,
      early_access: true,
      subscribe: true
    },
    successSend: false,
    failedSend: false
  },
  methods: {
    sendForm: function () {
      this.successSend = false
      this.failedSend = false
      var request = this.$http.post(
        'https://api.chentsovo.ru/v1/contact-form/',
        this.form
      ).then(response => {
        console.log(response.data)
        this.successSend = true
      }, response => {
        console.log(response.data)
        this.failedSend = true
      });
    }
  }
})